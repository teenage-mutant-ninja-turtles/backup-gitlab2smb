#!/bin/bash
# backup-gitlab2smb.sh
# exit status:
#  0 - Ok 
# 10 - file /etc/cifspasswd not found 
# 20 - ошибка копирования

#set -o xtrace
#set -o errexit
#set -o nounset
#set -o pipefail

COPYDAY=(30 3 2 1) #сколько дней разница с пследней копией
BACKUPSERVER=""
BACKUPSERVERSHARE="//${BACKUPSERVER}/rukbat"
DIR="/mnt/${BACKUPSERVER}"

BACKUPDIR="${DIR}/$(date +%F)"

. syslog-api.sh #эмуляция syslog
ZABBIX_CONF="/etc/zabbix/zabbix_agentd.conf"
ZABBIX_HOST_NAME=''
function zs_backup_completed
{
	local MESSAGE="${ZABBIX_HOST_NAME} backup_completed $(date +%s) $1"
	echo "${MESSAGE}" | zabbix_sender --config ${ZABBIX_CONF} -vv --with-timestamps --input-file -
}

openlog `basename "$0"` pid local3

syslog info "Backup start time: $(date)"

if [[ ! -d "${DIR}" ]]
then
	mkdir -p "${DIR}"
fi

#Mount backup share
if [[ "$(mount | grep ${DIR} | wc -l)" -gt "0" ]]
then
	#
	syslog notice "Umount ${DIR}"
	umount "${DIR}"
fi

if [[ ! -e "/etc/cifspasswd" ]]
then
	syslog crit "Error file /etc/cifspasswd not found"
	zs_backup_completed false
	closelog
	exit 10
fi
syslog info "Mount backup share ${BACKUPSERVERSHARE} to ${DIR}"
mount.cifs "${BACKUPSERVERSHARE}" "${DIR}" -o uid=0,gid=0,rw,credentials=/etc/cifspasswd

#Резервное копирование файлов
if [[ ! -d "${BACKUPDIR}" ]]
then
	mkdir -p "${BACKUPDIR}"
fi

gitlab-rake gitlab:backup:create
rsync -rlptD --stats '/etc/gitlab' "${BACKUPDIR}" &
rsync -rlptD --stats '/var/opt/gitlab/backups' "${BACKUPDIR}" &
wait
find /var/opt/gitlab/backups/*_gitlab_backup.tar -mmin +5 -delete
gitlab-rake gitlab:env:info > "${BACKUPDIR}/info.txt"

if [[ "$?" -ne "0" ]]
then
	# ошибка копирования
	syslog crit "Error $?"
	zs_backup_completed false
	closelog
	exit 20
fi

#Удаление старых копий
START_I=0
COPYDAY+=(0) #для того чтобы удалить копии между последней и копией последней с заданной разницей
for DAY in ${COPYDAY[*]};
do
	BACKUPS=(${DIR}/*) # массив резервных копий
	for ((i="${START_I}"; i<"${#BACKUPS[*]}"; i++))
	do
		d=(`date +%s --date="${BACKUPS[-1]##*/}"`-`date +%s --date="${BACKUPS[i]##*/}"`)/86400
		if [[ "${d}" -gt "${DAY}" ]]
		then
			# удаление старой копии
			syslog notice "find ${BACKUPS[i]} -maxdepth 0 -type d -exec rm -r {} \;"
			find "${BACKUPS[i]}" -maxdepth 0 -type d -exec rm -r {} \;
		fi
	done
	unset BACKUPS
	START_I=${START_I}+1
done

zs_backup_completed true

syslog info "Umount backup share ${DIR}"
umount "${DIR}"

syslog info "Backup finish time: $(date)"
closelog

exit 0